import string

class Node:
    def __init__(self, value = None):
        self.left = None
        self.right = None
        self.value = value
    def __str__(self):
        return (f'\{Node({self.value}): value = {self.value}, left = {self.left}, right = {self.right}\}')

nodes = {letter: Node(letter) for letter in string.ascii_lowercase}

def setNodes():
    pass

print(nodes)